package com.hztech.singapore.DAL;


import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by HzTech on 7/27/17.
 */

public class Attendence extends RealmObject {

    public String getLocation() {
        return location;
    }

    public String getmTime() {
        return mTime;
    }

    public String getmCamera() {
        return mCamera;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public void setmTime(String mTime) {
        this.mTime = mTime;
    }

    public void setmCamera(String mCamera) {
        this.mCamera = mCamera;
    }

    String location;
    @PrimaryKey
    String mTime;
    String mCamera;

}
