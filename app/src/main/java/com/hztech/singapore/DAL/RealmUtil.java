package com.hztech.singapore.DAL;

import android.support.annotation.NonNull;
import android.text.TextUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmResults;

/**
 * Created by HzTech on 7/27/2017.
 **/

public class RealmUtil {

    private static Realm createRealm() {
        return Realm.getInstance(new RealmConfiguration.Builder().build());
    }

    private static void closeRealm() {
        createRealm().close();
    }

    public static void saveAttendence(final Attendence comingAttendes, Realm.Transaction.OnSuccess comingOnSucess, Realm.Transaction.OnError comingOnError) {
        createRealm().executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                    realm.copyToRealmOrUpdate(comingAttendes);
                }

        },comingOnSucess,comingOnError);
        closeRealm();
    }

    public static ArrayList<Attendence> getAttendencess() {
        ArrayList<Attendence> Attenedes = new ArrayList<>();
        RealmResults<Attendence> AttendencessRealmList = createRealm().where(Attendence.class).findAll();
        for (int lanCount = 0; lanCount < AttendencessRealmList.size(); lanCount++) {
            Attenedes.add(AttendencessRealmList.get(lanCount));
        }
        closeRealm();
        return Attenedes;
    }

}