package com.hztech.singapore.utilities;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;


import com.hztech.singapore.interfaces.OperationListener;

import java.util.ArrayList;

import static android.content.pm.PackageManager.PERMISSION_GRANTED;

public class PermissionUtil {
    private PermissionUtil() {
    }

    private static ArrayList<String> grantedPermissions, deniedPermissions;
    private static final String CAMERA_PERMISSION = Manifest.permission.CAMERA;
    private static final String STORAGE_PERMISSION = Manifest.permission.READ_EXTERNAL_STORAGE;
    private static final String CORSE_LOCATION = Manifest.permission.ACCESS_COARSE_LOCATION;
    private static final String FINE_LOCATION = Manifest.permission.ACCESS_FINE_LOCATION;
    private static final int PERMISSION_REQUEST_CODE = 1024;
    private static final String[] permissions = {STORAGE_PERMISSION, CAMERA_PERMISSION,CORSE_LOCATION,FINE_LOCATION};
    private static boolean showAction=false;
    private static OperationListener mOperationListener;
    private static ArrayList<String> activityPermissions(@NonNull Context activity,
                                                         @NonNull String[] permissions, int requestCode) {

        grantedPermissions = new ArrayList<>();
        deniedPermissions = new ArrayList<>();
        for (String permission : permissions) {
            int permissionCheck = ContextCompat.checkSelfPermission(activity, permission);
            if (permissionCheck == PERMISSION_GRANTED) {
                grantedPermissions.add(permission);
            } else {
                deniedPermissions.add(permission);
            }
        }
        if (grantedPermissions.size() != permissions.length) {
            ActivityCompat.requestPermissions((Activity) activity,
                    deniedPermissions.toArray(new String[deniedPermissions.size()]), requestCode);
        }
        if(grantedPermissions.size()==getPermissions().length){
            showAction=true;
        }else{
            showAction=false;
        }
        return grantedPermissions;
    }

    public static boolean isShowAction(){
        return showAction;
    }
//    public static ArrayList<String> fragmentPermissions(@NonNull Fragment fragment,
//                                                        @NonNull String[] permissions, int requestCode) {
//        ArrayList<String> grantedPermissions = new ArrayList<>(),
//                deniedPermissions = new ArrayList<>();
//        for (String permission : permissions) {
//            int permissionCheck = ContextCompat.checkSelfPermission(fragment.getActivity(), permission);
//            if (permissionCheck == PERMISSION_GRANTED) {
//                grantedPermissions.add(permission);
//            } else {
//                deniedPermissions.add(permission);
//
//            }
//        }
//        if (grantedPermissions.size() != permissions.length) {
//            FragmentCompat.requestPermissions(fragment,
//                    deniedPermissions.toArray(new String[deniedPermissions.size()]), requestCode);
//        }
//        return grantedPermissions;
//    }

    public static ArrayList<String> getGrantedPermissions() {
        return grantedPermissions;
    }

    public static void askForNeededPermissions(Context comingContext, OperationListener comingOperationListener){
        mOperationListener=comingOperationListener;
        if(checkAPILevel()){
            grantedPermissions = activityPermissions(comingContext, permissions, PERMISSION_REQUEST_CODE);
            if(isShowAction()){
                mOperationListener.onOperationCompleted(1,"",comingContext);
            }else{
//                mOperationListener.onOperationCompleted(0,"");
            }
        }else{
            mOperationListener.onOperationCompleted(0,"notm",comingContext);
        }
    }

    public static boolean checkAPILevel() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.M;
    }
    public static boolean checkLolipopAPILevel() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP;
    }
    public static String getPermissionsString() {
        String persmission = "";
        for (String mstirng : permissions) {
            persmission += mstirng + ", ";
        }
        return persmission;
    }

    public static void onRequestPermissionResult(int requestCode, @NonNull String[] comingPermissions, @NonNull int[] grantResults) {
        boolean permissionNotGranted=false;
        if (requestCode == PERMISSION_REQUEST_CODE) {
            if(grantResults.length==comingPermissions.length){
                for(int i=0;i<grantResults.length;i++){
                    if(grantResults[i]!=PERMISSION_GRANTED){
                        permissionNotGranted=true;
                        break;
                    }
                }
                if(permissionNotGranted){
                    mOperationListener.onOperationCompleted(0,getStrings(comingPermissions),null);
                }else{
                    mOperationListener.onOperationCompleted(1,"completed",null);
                }
            }
//            else{
//                mOperationListener.onOperationCompleted(0,getStrings(comingPermissions));
//            }
        }
    }

    private static String[] getPermissions(){
        return permissions;
    }

    private static String getStrings(String[] strings){
        String s="";
        for (int i=0;i<strings.length;i++){
            s+=strings[i];
        }
        return s;
    }
}