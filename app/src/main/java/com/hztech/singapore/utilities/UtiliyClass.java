package com.hztech.singapore.utilities;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hztech.singapore.R;

/**
 * Created by HzTech on 7/25/17.
 */

public class UtiliyClass {
    private static Snackbar snackbar;

    public static void showSnackToast(String message,Context comingContext) {
        showAlert(comingContext,message,comingContext.getString(R.string.ok), null, null, null, null, null, null,0);
    }

    public static void ShowSnackToastWithImage(String message,int comingImage, Context comingContext){
        showAlert(comingContext,message, comingContext.getString(R.string.ok), null, null, null, null, null, null, comingImage);
    }


    public static void showRealSnackToast(String message, Context comingContext) {
        snackbar = Snackbar.make(((Activity)comingContext).findViewById(android.R.id.content), message, Snackbar.LENGTH_LONG);
        snackbar.getView().setBackgroundColor(comingContext.getResources().getColor(R.color.redDark));
        snackbar.setAction(R.string.ok, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                snackbar.dismiss();
            }
        });
        snackbar.setActionTextColor(comingContext.getResources().getColor(R.color.hintTextColor));
        snackbar.show();
    }

    public void showSnackToast(String message, int actionText, final Context comingContext, final View.OnClickListener comingListener) {
//        snackbar = Snackbar.make(findViewById(android.R.id.content), message, Snackbar.LENGTH_LONG);
//        snackbar.setAction(actionText, comingListener);
//        snackbar.setActionTextColor(getResources().getColor(R.color.hintTextColor));
//        snackbar.show();
        showAlert(comingContext,message, comingContext.getString(actionText), null, null, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                comingListener.onClick(new TextView(comingContext));
            }
        }, null, null, null, 0);
    }

    public static void showCustomSnackToast(final Context comingContext, String message, String actionText, final View.OnClickListener comingListener) {
//        snackbar = Snackbar.make(findViewById(android.R.id.content), message, Snackbar.LENGTH_INDEFINITE);
//        Snackbar.SnackbarLayout layout = (Snackbar.SnackbarLayout) snackbar.getView();
//        TextView textView = (TextView) layout.findViewById(android.support.design.R.id.snackbar_text);
//        // Inflate our custom view
//        View snackView = LayoutInflater.from(comingContext).inflate(R.layout.my_snackbar, null);
//        // Configure the view
//        ImageView imageView = (ImageView) snackView.findViewById(R.id.btn_snack_cancel);
//        TextView textViewTop = (TextView) snackView.findViewById(R.id.lbl_snack_content);
//        textViewTop.setText(message);
//        textViewTop.setTextColor(Color.WHITE);
//
//        Button mButton = (Button) snackView.findViewById(R.id.btn_snack_ok);
//        mButton.setText(actionText);
//        mButton.setTextColor(comingContext.getResources().getColor(R.color.actionColor));
//        mButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                comingListener.onClick(view);
//                snackbar.dismiss();
//            }
//        });
//        imageView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                snackbar.dismiss();
//                enableRequestButtons(true);
//            }
//        });
//// Add the view to the Snackbar's layout
//        layout.addView(snackView, 0);
//        textView.setVisibility(View.INVISIBLE);
//// Show the Snackbar
//        snackbar.show();
        showAlert(comingContext,message, actionText, comingContext.getString(R.string.cancel), null, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                comingListener.onClick(new TextView(comingContext));
                dialogInterface.dismiss();
            }
        }, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        }, null, null, 0);
    }

    /**
     * Shows an AlertDialog to the user.
     *
     * @param message                Text message to set as the message for the {@link AlertDialog}.
     * @param positiveButtonTitle    Title for the positive Button. (Pass null if you don't want this button)
     * @param negativeButtonTitle    Title for the negative Button. (Pass null if you don't want this button)
     * @param neutralButtonTitle     Title for the neutral Button. (Pass null if you don't want this button)
     * @param positiveButtonListener {@link AlertDialog.OnClickListener} to the positive button.
     * @param negativeButtonListener {@link AlertDialog.OnClickListener} to the negative button.
     *                               (Pass null if you want the default {@link DialogInterface#cancel()})
     * @param neutralButtonListener  {@link AlertDialog.OnClickListener} to the neutral button.
     * @param comingImageResousnce
     * @see {@link AlertDialog#BUTTON_POSITIVE}
     * @see {@link AlertDialog#BUTTON_NEGATIVE}
     * @see {@link AlertDialog#BUTTON_NEUTRAL}
     */
    public static void showAlert(Context comingContext, String message,
                                 String positiveButtonTitle, String negativeButtonTitle, String neutralButtonTitle,
                                 DialogInterface.OnClickListener positiveButtonListener,
                                 DialogInterface.OnClickListener negativeButtonListener,
                                 DialogInterface.OnClickListener neutralButtonListener,
                                 String mDialogTitle, int comingImageResousnce) {
        Button tempButton;
        TextView txt;
        ImageView mImageView;
        AlertDialog.Builder mBuilder = new AlertDialog.Builder(comingContext);
        if (mDialogTitle != null) {
//            TextView mTitle = new TextView(this);
//            mTitle.setText(mDialogTitle);
//            mBuilder.setCustomTitle(mTitle);
            mBuilder.setTitle(mDialogTitle);
        }
        AlertDialog alertDialog = mBuilder.create();

        LayoutInflater li = alertDialog.getLayoutInflater();
        View view = li.inflate(R.layout.alert_layout, null);
        txt = (TextView) view.findViewById(R.id.text_alertLayout);
        mImageView = (ImageView) view.findViewById(R.id.img_alert_image);
        alertDialog.setView(view);

        txt.setText(message);
        if(comingImageResousnce!=0){
            mImageView.setVisibility(View.VISIBLE);
            mImageView.setImageResource(comingImageResousnce);
        }

        if (positiveButtonTitle != null) {
            alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE, positiveButtonTitle, positiveButtonListener);
        }

        if (negativeButtonTitle != null) {
            DialogInterface.OnClickListener cancelAction;
            if (negativeButtonListener == null) {
                cancelAction = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                };
            } else
                cancelAction = negativeButtonListener;

            alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, negativeButtonTitle, cancelAction);
        }

        if (neutralButtonTitle != null) {
            alertDialog.setButton(DialogInterface.BUTTON_NEUTRAL, neutralButtonTitle, neutralButtonListener);
        }


        alertDialog.show();

        tempButton = alertDialog.getButton(DialogInterface.BUTTON_POSITIVE);
        if (tempButton != null) {
            LinearLayout.LayoutParams params = (android.widget.LinearLayout.LayoutParams) tempButton.getLayoutParams();
            params.setMargins(3, 3, 5, 0);

        }
        tempButton = alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE);
        if (tempButton != null) {
            LinearLayout.LayoutParams params = (android.widget.LinearLayout.LayoutParams) tempButton.getLayoutParams();
            params.setMargins(5, 3, 3, 0);

        }
        tempButton = alertDialog.getButton(AlertDialog.BUTTON_NEUTRAL);
        if (tempButton != null) {
            LinearLayout.LayoutParams params = (android.widget.LinearLayout.LayoutParams) tempButton.getLayoutParams();
            params.setMargins(5, 3, 5, 0);

        }
    }

    public static boolean checkForInternetConnectivity(Context mContext){
        ConnectivityManager cm =
                (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }
}
