package com.hztech.singapore;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.hztech.singapore.interfaces.LocationSettingsRequestsInterface;

import io.realm.Realm;
import pl.aprilapps.easyphotopicker.EasyImage;

public class MainActivity extends AppCompatActivity {
    public LocationSettingsRequestsInterface mLocationListion;
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {

            switch (item.getItemId()) {
                case R.id.navigation_attendence:
                    addContentFragment(new MainActivityFragment(),false);
                    return true;
                case R.id.navigation_history:
                  addContentFragment(new HistoryFragment(),false);
                    return true;
            }
            return false;
        }
    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        Realm.init(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    public void setLocationSettingListener(LocationSettingsRequestsInterface mListener) {
        mLocationListion = mListener;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case 5000:      //LocationSettingsRequests
                if (mLocationListion != null) {
                    mLocationListion.onRequestResult(requestCode, resultCode, data);
                }
                break;
            case EasyImage.REQ_TAKE_PICTURE:
                FragmentManager mManager=getSupportFragmentManager();
                Fragment mFragments=(Fragment) mManager.findFragmentById(R.id.fragment);
                ((MainActivityFragment)mFragments).onActivityResult(requestCode,resultCode,data);
                break;
        }
    }

    public void addContentFragment(Fragment ComingFragment,boolean comingAddToBackStack){
        FragmentManager fm=getSupportFragmentManager();
        FragmentTransaction fragmentTransaction=fm.beginTransaction();
        fragmentTransaction.replace(R.id.fragment, ComingFragment,ComingFragment.getClass().toString());
        if(comingAddToBackStack){
            fragmentTransaction.addToBackStack(ComingFragment.getClass().toString());
        }
        fragmentTransaction.commitAllowingStateLoss();
    }
}
