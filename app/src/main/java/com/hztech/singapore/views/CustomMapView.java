package com.hztech.singapore.views;

/**
 * Created by Hztech on 7/25/2017
 */

import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.net.Uri;
import android.provider.Settings;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMapOptions;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.hztech.singapore.MainActivity;
import com.hztech.singapore.R;
import com.hztech.singapore.interfaces.OperationListener;
import com.hztech.singapore.utilities.PermissionUtil;
import com.hztech.singapore.utilities.UtiliyClass;

public class CustomMapView extends MapView {
    OperationListener mOperationListener;

    public CustomMapView(Context context) {
        super(context);
        init();
    }

    public CustomMapView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CustomMapView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public CustomMapView(Context context, GoogleMapOptions options) {
        super(context, options);
        init();
    }

    private void init() {
        LayoutInflater.from(getContext()).inflate(R.layout.custom_map_view, this, true);
        getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(final GoogleMap googleMap) {
                PermissionUtil.askForNeededPermissions(getContext(), new OperationListener() {
                    @Override
                    public void onOperationCompleted(int resultCode, Object mComingValue, Context comingContext) {
                        if (resultCode == 1) {                                                                                                         //above or= marsh and permissions  granted
//                    Toast.makeText(getActivity(), "Should SHow the imagePicker", Toast.LENGTH_SHORT).show();
                            intiMap(googleMap);
                        } else {
                            if (((String) mComingValue).equalsIgnoreCase("notm")) {                        //below marsh
//                        Toast.makeText(getActivity(), "Should SHow the imagePicker", Toast.LENGTH_SHORT).show();
                                intiMap(googleMap);
                            } else {                                                                                                                              //above or= marsh and permissions not granted
                                UtiliyClass.showCustomSnackToast(getContext(), getContext().getString(R.string.please_allow_permission) + mComingValue, getContext().getString(R.string.ok), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        Intent intent = new Intent();
                                        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                        Uri uri = Uri.fromParts("package",
                                                getContext().getPackageName(), null);
                                        intent.setData(uri);
                                        getContext().startActivity(intent);
                                    }
                                });
                            }
                        }
                    }
                });
//                setMarkers("Hello");
            }
        });

    }

    private void intiMap(GoogleMap googleMap){
        googleMap.getUiSettings().setTiltGesturesEnabled(true);
        googleMap.getUiSettings().setRotateGesturesEnabled(true);
        googleMap.setMyLocationEnabled(true);
        googleMap.getUiSettings().setMyLocationButtonEnabled(false);
        googleMap.setTrafficEnabled(false);
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(30.064137, 31.306428), 1.0f));
        if (mOperationListener != null) {
            mOperationListener.onOperationCompleted(1, null,getContext());
        }
    }

    public void setMarkers(final String mComingString) {
        getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                googleMap.addMarker(new MarkerOptions()
                        .icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_launcher))
//                .anchor(0.0f, 1.0f) // Anchors the marker on the bottom left
                        .position(new LatLng(30.064137, 31.306428))).setTitle(mComingString);
            }
        });
    }

    public void setMarkers(final String mComingString, final Location mComingLocation, final int mResourcesID) {
        getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                googleMap.addMarker(new MarkerOptions()
                        .icon(BitmapDescriptorFactory.fromResource(mResourcesID))
//                .anchor(0.0f, 1.0f) // Anchors the marker on the bottom left
                        .position(new LatLng(mComingLocation.getLatitude(), mComingLocation.getLongitude()))).setTitle(mComingString);
            }
        });
    }

    public void setMarkers(final MarkerOptions mComingMarkerOptions) {
        getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                googleMap.addMarker(mComingMarkerOptions);
            }
        });
    }

    public void setmOperationListener(OperationListener mComingOperationListener) {
        this.mOperationListener = mComingOperationListener;
    }

    public OperationListener getmOperationListener() {
        return this.mOperationListener;
    }
}