package com.hztech.singapore;

import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.net.Uri;
import android.os.Handler;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.hztech.singapore.DAL.Attendence;
import com.hztech.singapore.DAL.RealmUtil;
import com.hztech.singapore.interfaces.LocationSettingsRequestsInterface;
import com.hztech.singapore.interfaces.OperationListener;
import com.hztech.singapore.managers.GeocodingService;
import com.hztech.singapore.managers.LocationManager;
import com.hztech.singapore.utilities.PermissionUtil;
import com.hztech.singapore.utilities.UtiliyClass;
import com.hztech.singapore.views.CustomMapView;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Calendar;

import io.realm.Realm;
import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;

import static com.hztech.singapore.managers.GeocodingService.LOCATN_KEY;

/**
 * A placeholder fragment containing a simple view.
 */
public class MainActivityFragment extends Fragment implements OperationListener {
    View mFragmentView;
    CustomMapView mMapView;                                                             //Map view to show map
    private Location mCurrentLocation;                                                   //current location coordinates
    Button mAddressLbl;                                                                           //current geocde value of current location
    Button mCameraText;                                                                         //camera Taken picture
    Button mSaveButton;                                                                          //SaveButton for history;
    ImageButton mCameraButton;
    String mImageTakenPath;
    LocationSettingsRequestsInterface mLocationSettingRequestInterface;
    /**
     * used to move camera to user position on map only first time to start fragment
     */
    private boolean mapFirstTime = false;

    public MainActivityFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mFragmentView = inflater.inflate(R.layout.fragment_main, container, false);
        mMapView = mFragmentView.findViewById(R.id.mapview_home_layout);
        mMapView.onCreate(savedInstanceState);
        mAddressLbl = (Button) mFragmentView.findViewById(R.id.lbl_main_fragment_address);
        mCameraButton = (ImageButton) mFragmentView.findViewById(R.id.btn_main_fragment_camera);
        mCameraText  = (Button) mFragmentView.findViewById(R.id.lbl_main_fragment_text);
        mSaveButton = (Button) mFragmentView.findViewById(R.id.btn_main_fragment_save);

        mCameraButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EasyImage.openCamera(((MainActivity)getActivity()), 0);
            }
        });

        mCameraText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //TODO should show Taken Picture
//                ((MainActivity)getActivity()).addContentFragment(new ImageFragment(),true);
            }
        });

        mSaveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!mAddressLbl.getText().toString().isEmpty()) {
//                    if(!mImageTakenPath.isEmpty()) {
                        Attendence mAttendence = new Attendence();
                        mAttendence.setLocation(mAddressLbl.getText().toString());
                        mAttendence.setmTime(Calendar.getInstance().getTimeInMillis()+"");
                        mAttendence.setmCamera(mImageTakenPath);
                    RealmUtil.saveAttendence(mAttendence, new Realm.Transaction.OnSuccess() {
                        @Override
                        public void onSuccess() {
                            Toast.makeText(getActivity(), "Record Saved", Toast.LENGTH_SHORT).show();
                        }
                    }, new Realm.Transaction.OnError() {
                        @Override
                        public void onError(Throwable error) {
                            Toast.makeText(getActivity(), "Error Saving the Record", Toast.LENGTH_SHORT).show();
                        }
                    });
//                    }else{
//                        Toast
                    }else{
                    Toast.makeText(getActivity(), "please wait", Toast.LENGTH_SHORT).show();
                }
            }
        });
        //Location geocodeing
        LocationManager.getInstance(getActivity()).setOperationListener((OperationListener) this);
        mAddressLbl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    mMapView.getMapAsync(new OnMapReadyCallback() {
                        @Override
                        public void onMapReady(final GoogleMap googleMap) {
                            if (googleMap != null) {

                                PermissionUtil.askForNeededPermissions(getActivity(), new OperationListener() {
                                    @Override
                                    public void onOperationCompleted(int resultCode, Object mComingValue,Context comingContext) {
                                        if (resultCode == 1) {                                                                                                         //above or= marsh and permissions  granted
//                    Toast.makeText(getActivity(), "Should SHow the imagePicker", Toast.LENGTH_SHORT).show();
                                            googleMap.setMyLocationEnabled(true);
                                            if (LocationManager.getInstance(getActivity()).getmLastLocation() != null) {
                                                googleMap.moveCamera(CameraUpdateFactory.newLatLng(
                                                        new LatLng(LocationManager.getInstance(getActivity()).getmLastLocation().getLatitude(),
                                                                LocationManager.getInstance(getActivity()).getmLastLocation().getLongitude())));
                                            }else{
                                                mLocationSettingRequestInterface = (LocationSettingsRequestsInterface) LocationManager.getInstance(getActivity()).setMapView(mMapView).buildGoogleMapApiClient(MainActivityFragment.this);
                                                mapFirstTime=false;
                                            }
                                        } else {
                                            if (((String) mComingValue).equalsIgnoreCase("notm")) {                        //below marsh
                                                googleMap.setMyLocationEnabled(true);
                                                if (LocationManager.getInstance(getActivity()).getmLastLocation() != null) {
                                                    googleMap.moveCamera(CameraUpdateFactory.newLatLng(
                                                            new LatLng(LocationManager.getInstance(getActivity()).getmLastLocation().getLatitude(),
                                                                    LocationManager.getInstance(getActivity()).getmLastLocation().getLongitude())));
                                                }else{
                                                    mLocationSettingRequestInterface = (LocationSettingsRequestsInterface) LocationManager.getInstance(getActivity()).setMapView(mMapView).buildGoogleMapApiClient(MainActivityFragment.this);
                                                    mapFirstTime=false;
                                                }
                                            } else {                                                                                                                              //above or= marsh and permissions not granted
                                                UtiliyClass.showCustomSnackToast(getActivity(), getActivity().getString(R.string.please_allow_permission) + mComingValue, getActivity().getString(R.string.ok), new View.OnClickListener() {
                                                    @Override
                                                    public void onClick(View v) {
                                                        Intent intent = new Intent();
                                                        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                                        Uri uri = Uri.fromParts("package",
                                                                getActivity().getPackageName(), null);
                                                        intent.setData(uri);
                                                        getActivity().startActivity(intent);
                                                    }
                                                });
                                            }
                                        }
                                    }
                                });

                            }
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });
        mLocationSettingRequestInterface = (LocationSettingsRequestsInterface) LocationManager.getInstance(getActivity()).setMapView(mMapView).buildGoogleMapApiClient(this);
        LocationManager.getInstance(getActivity()).setMapView(mMapView).startLocationGeocodingService(mMapView);

        //Camera
        EasyImage.configuration(getActivity())
                .setImagesFolderName("attendenceDemo")
                .saveInAppExternalFilesDir()
                .setCopyExistingPicturesToPublicLocation(true);


        mAddressLbl.performClick();
        return mFragmentView;
    }

    @Override
    public void onResume() {
        super.onResume();
        mMapView.onResume();
        mapFirstTime = false;
        ((MainActivity) getActivity()).setLocationSettingListener(mLocationSettingRequestInterface);
        LocationManager.getInstance(getActivity()).setOperationListener((OperationListener) this);
        LocationManager.getInstance(getActivity()).setGeocodeOperationFailureListener(new OperationListener() {
            @Override
            public void onOperationCompleted(int resultCode, Object mComingValue, Context mContext) {

            }
        });


    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
        LocationManager.getInstance(getActivity()).onDestroy();
    }

    @Override
    public void onPause() {
        super.onPause();
        mMapView.onPause();
//        LocationManager.getInstance(mContext).onPause();
    }

    private void verifyCity(Location comingLocation, final boolean callForGeocode){
        LocationManager.getInstance(getActivity()).verifyCity(comingLocation,callForGeocode, new OperationListener() {
            @Override
            public void onOperationCompleted(int resultCode, Object mComingValue, Context mContext) {
                if (resultCode == 1) {
                    if(callForGeocode){
                        mAddressLbl.setText((String)mComingValue);
                    }
                }else if (resultCode==0){
                    mAddressLbl.setText(mComingValue.toString());
                }
            }
        });
    }
    @Override
    public void onOperationCompleted(int resultCode, Object mComingValue, Context comingContext) {
//        if(resultCode== GeocodingService.RESULT_SUCCESS){
        if (resultCode != 3) {
            if (getActivity() != null) {
                if (mComingValue instanceof Bundle) {
                    if(resultCode == GeocodingService.RESULT_SUCCESS){
                        String mMessage = ((Bundle) mComingValue).getString(GeocodingService.RESULT_KEY);
                        mCurrentLocation = ((Bundle) mComingValue).getParcelable("Location");
                        mAddressLbl.setText(mMessage);
                        if (resultCode == GeocodingService.RESULT_SUCCESS) {
                            getActivity().invalidateOptionsMenu();
                        }
                    }else if(resultCode == GeocodingService.RESULT_FAILURR){
                        mCurrentLocation=(Location) ((Bundle)mComingValue).getParcelable(LOCATN_KEY);
                        verifyCity(mCurrentLocation,true);
                    }
                } else if (mComingValue instanceof Location) {
                    mCurrentLocation = (Location) mComingValue;
                    mMapView.getMapAsync(new OnMapReadyCallback() {
                        @Override
                        public void onMapReady(GoogleMap googleMap) {
                            googleMap.setMyLocationEnabled(true);
                            if (!mapFirstTime) {
                                googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude()), 17.0f));
                                mapFirstTime = true;
                                LocationManager.getInstance(getActivity()).startGeocodingService(mCurrentLocation, "", true);
                            }
                        }
                    });
                }
            }
        }
//        else if (resultCode == 3) {
//            verifyCity((Location) mComingValue,true);
//        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Log.d(this.getClass().getSimpleName(),"****************onActivityResult*****************");
        EasyImage.handleActivityResult(requestCode, resultCode, data, getActivity(), new DefaultCallback() {
            @Override
            public void onImagePickerError(Exception e, EasyImage.ImageSource source, int type) {
                //Some error handling
                Log.d(this.getClass().getSimpleName(),"****************onImagePickerError*****************");
                e.printStackTrace();
            }

            @Override
            public void onImagePicked(final File imageFile, EasyImage.ImageSource source, int type) {
                //Handle the image
                Log.d(this.getClass().getSimpleName(),"onImagePicked........"+imageFile.getPath());

                mImageTakenPath = imageFile.getPath();
                mCameraText.setText("Image:"+mImageTakenPath);
            }
        });
    }
}
