package com.hztech.singapore.managers;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.os.ResultReceiver;

import com.hztech.singapore.interfaces.OperationListener;


/**
 * Created by HzTech on 7/25/2016.
 */
public class GeocodingResultReceiver extends ResultReceiver {
    private static GeocodingResultReceiver mReceiver;
    private OperationListener operationListener;

    @SuppressLint("RestrictedApi")
    public GeocodingResultReceiver(Handler mHandler) {
        super(mHandler);
    }

    public static GeocodingResultReceiver getInstance() {
        if (mReceiver == null) {
            mReceiver = new GeocodingResultReceiver(new Handler());
        }
        return mReceiver;
    }

    @Override
    protected void onReceiveResult(int resultCode, Bundle resultData) {
//        String mMessage=resultData.getString(GeocodingService.RESULT_KEY);
//        if(resultCode==GeocodingService.RESULT_SUCCESS){
        //Update the Ui View;
        operationListener.onOperationCompleted(resultCode, resultData,null);
//        }

    }

    public void setOperationListener(OperationListener operationListener) {
        this.operationListener = operationListener;
    }
}