package com.hztech.singapore.managers;

import android.app.IntentService;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.os.ResultReceiver;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;

import com.hztech.singapore.R;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p>
 * TODO: Customize class - update intent actions, extra parameters and static
 * helper methods.
 */
public class GeocodingService extends IntentService {
    public static final int RESULT_SUCCESS = 1;
    public static final int RESULT_FAILURR = 0;
    public static final String TEXT_KEY = "com.googan.lutfipassenger.TEXT";
    public static final String RESULT_KEY = "com.googan.lutfipassenger.RESULT_KEY";
    public static final String RECIVR_KEY = "com.googan.lutfipassenger.RECEIVER";
    public static final String LOCATN_KEY = "com.googan.lutfipassenger.LOCATION";
    public static final String COUNTRY_CODE_KEY = "COUNTRY_CODE_KEY";
    private static final String TAG = "GeocodingService";
    private ArrayList<String> filterCountries;
    protected ResultReceiver mReceiver;
    Location mLastLocation;
    public GeocodingService() {
        super("GeocodingService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            mLastLocation = intent.getParcelableExtra(LOCATN_KEY);
            String mText = intent.getStringExtra(TEXT_KEY);
            mReceiver = intent.getParcelableExtra(RECIVR_KEY);
            filterCountries = new ArrayList<>();
            filterCountries.add("EG");
            filterCountries.add("SA");
            filterCountries.add("LB");

            if (action.equalsIgnoreCase(LOCATN_KEY))
                handleGeocoding(mLastLocation.getLongitude(), mLastLocation.getLatitude());
            else if (action.equalsIgnoreCase(TEXT_KEY))
                handleGeocoding(mText);
            else if (action.equalsIgnoreCase("CountryGeocode"))
                handleCountryGeocoding(mLastLocation.getLongitude(), mLastLocation.getLatitude());

        }
    }

    /**
     * Handle the geocoding process
     *
     * @param mComingLongitude .
     * @param mComingLatitude  .
     */
    private void handleGeocoding(double mComingLongitude, double mComingLatitude) {
        String ERROR = "";
        /*TelephonyManager tm = (TelephonyManager) getApplicationContext().getSystemService(TELEPHONY_SERVICE);
        String country = tm.getNetworkCountryIso();
        Locale locale = new Locale(Locale.getDefault().getLanguage(), country);*/
        Geocoder mGeocoder = new Geocoder(this/*, locale*/);
        List<Address> mAddress = null;

        try {
            mAddress = mGeocoder.getFromLocation(mComingLatitude, mComingLongitude, 1);
        } catch (IOException ioException) {
            // Catch network or other I/O problems.
            ERROR = getString(R.string.service_not_available);
            Log.e(TAG, ERROR, ioException);
        } catch (IllegalArgumentException illegalArgumentException) {
            // Catch invalid latitude or longitude values.
            ERROR = getString(R.string.invalid_lat_long_used);
            Log.e(TAG, ERROR + ". " +
                    "Latitude = " + mComingLatitude +
                    ", Longitude = " +
                    mComingLongitude, illegalArgumentException);
        }

        //if no Address found
        if (mAddress == null || mAddress.size() == 0) {
            if (ERROR.isEmpty()) {
                ERROR = getString(R.string.no_address_found);
                Log.e(TAG, ERROR);
            }
            resultToReceiver(RESULT_FAILURR, ERROR, null);
        } else {      //if Address found send back the address
            Address mAddres = mAddress.get(0);
            ArrayList<String> mAddressLines = new ArrayList<>();
            String City="";
            for (int i = 0; i < mAddres.getMaxAddressLineIndex(); i++) {
                mAddressLines.add(mAddres.getAddressLine(i));
                if(mAddres.getAddressLine(i).contains("Governorate")||mAddres.getAddressLine(i).contains("محافظة")||mAddres.getAddressLine(i).contains("Province")||mAddres.getAddressLine(i).contains("منطقة")) {
                    City =mAddres.getAddressLine(i).split(" ")[0];
                }
            }
//            Log.i(TAG, "=======================" + getString(R.string.address_found) +City);
            Location mPickupLocation = new Location("");
            mPickupLocation.setLatitude(mComingLatitude);
            mPickupLocation.setLongitude(mComingLongitude);
            resultToReceiver(RESULT_SUCCESS, TextUtils.join(" ", mAddressLines), mPickupLocation);
//            resultToReceiver(RESULT_SUCCESS, mAddres.getThoroughfare(), mPickupLocation);
        }
    }

    private void handleCountryGeocoding(double mComingLongitude, double mComingLatitude) {
        String ERROR = "";
        /*TelephonyManager tm = (TelephonyManager) getApplicationContext().getSystemService(TELEPHONY_SERVICE);
        String country = tm.getNetworkCountryIso();
        Locale locale = new Locale(Locale.getDefault().getLanguage(), country);*/
        Geocoder mGeocoder = new Geocoder(this/*, locale*/);
        List<Address> mAddress = null;

        try {
            mAddress = mGeocoder.getFromLocation(mComingLatitude, mComingLongitude, 1);
        } catch (IOException ioException) {
            // Catch network or other I/O problems.
            ERROR = getString(R.string.service_not_available);
            Log.e(TAG, ERROR, ioException);
        } catch (IllegalArgumentException illegalArgumentException) {
            // Catch invalid latitude or longitude values.
            ERROR = getString(R.string.invalid_lat_long_used);
            Log.e(TAG, ERROR + ". Latitude = " + mComingLatitude + ", Longitude = " + mComingLongitude, illegalArgumentException);
        }
        String mCountry = null;
        String mCity = null;
        //if no Address found
        if (mAddress == null || mAddress.size() == 0) {
            if (ERROR.isEmpty()) {
                ERROR = getString(R.string.no_address_found);
                Log.e(TAG, ERROR);
            }
        } else {      //if Address found send back the address
            Address mAddres = mAddress.get(0);
            mCountry = mAddres.getCountryCode();
            mCity = mAddres.getLocality();
            if(mCity==null||mCity.isEmpty()||mCity.equalsIgnoreCase("")) {
                mCity = mAddres.getAdminArea();
            }
            if(mCity==null){
                for(int i=0;i<mAddres.getMaxAddressLineIndex();i++){
                    if (mAddres.getAddressLine(i).contains("Governorate") ||mAddres.getAddressLine(i).contains("Province")) {
                        mCity = mAddres.getAddressLine(i).split(" ")[0];
                    }else if(mAddres.getAddressLine(i).contains("منطقة")|| mAddres.getAddressLine(i).contains("محافظة")){
                        mCity = mAddres.getAddressLine(i).split(" ")[1];
                    }
                }
            }
            LocationManager.getInstance(this).getCountryOperationListener().onOperationCompleted(1, mCountry+","+mCity, this);
            Log.i(TAG, "=======================" + " Country Found " + mCountry);
            Log.i(TAG, "=======================" + " City Found " + mCity);
        }
    }

    ///===============================================Not called so its not working.
    private void handleGeocoding(String mResult) {
        String ERROR = "";
        TelephonyManager tm = (TelephonyManager) getApplicationContext().getSystemService(TELEPHONY_SERVICE);
        String country = tm.getNetworkCountryIso();
        Locale locale = new Locale(Locale.getDefault().getLanguage(), country);
        Geocoder mGeocoder = new Geocoder(this, locale);
        Log.d("LOCALE", locale.getCountry());
        List<Address> mAddresses = null;
        try {
//            mAddresses=mGeocoder.getFromLocationName(mResult,3,29.787039,30.920778,30.164757,31.471634);  //for egypt
//            mAddresses = mGeocoder.getFromLocationName(mResult, 3, 16.404378, 42.767887, 28.534293, 48.430524); // for Soudia
            Log.d("START", System.currentTimeMillis() + "");
            mAddresses = mGeocoder.getFromLocationName(mResult + " " + country, 10); //for All World
            Log.d("END", System.currentTimeMillis() + "");
        } catch (IOException ioException) {
            // Catch network or other I/O problems.
            ERROR = getString(R.string.service_not_available);
            Log.e(TAG, ERROR, ioException);
        } catch (IllegalArgumentException illegalArgumentException) {
            // Catch invalid latitude or longitude values.
            ERROR = getString(R.string.invalid_lat_long_used);
            Log.e(TAG, ERROR + ". " +
                    "String " + mResult, illegalArgumentException);
        }

        //if no Address found
        if (mAddresses == null || mAddresses.size() == 0 || !filterList(mAddresses)) {
            if (ERROR.isEmpty()) {
                ERROR = getString(R.string.no_address_found);
                Log.e(TAG, ERROR);
            }
            resultToReceiver(RESULT_FAILURR, ERROR, null);
        } else {      //if Address found send back the address
//            Address mAddres=mAddresses.get(0);
//            ArrayList<String> mAddressLines=new ArrayList<>();
//            for(int i = 0; i < mAddres.getMaxAddressLineIndex(); i++) {
//                mAddressLines.add(mAddres.getAddressLine(i));
//            }
//            Log.i(TAG,"=======================" + getString(R.string.address_found)+TextUtils.join(" ",
//                    mAddressLines));
            Location mDestination = new Location("");
//            mPickupLocation.setLongitude(mComingLatitude);
//            mPickupLocation.setLongitude(mComingLongitude);
            resultToReceiver(RESULT_SUCCESS, mAddresses, mDestination);
        }
    }

    private boolean filterList(List<Address> addresses) {
        for (int address = 0; address < addresses.size(); address++) {
            boolean delete = true;
            String countryCode = addresses.get(address).getCountryCode();
            for (int filter = 0; filter < filterCountries.size(); filter++) {
                delete &= !(!TextUtils.isEmpty(countryCode) && countryCode.equalsIgnoreCase(filterCountries.get(filter)));
            }
            Log.d("DELETE", delete + "");
            if (delete) {
                addresses.remove(address);
            }
        }
        return addresses.size() != 0;
    }


    @SuppressWarnings("unchecked")
    private void resultToReceiver(int resultCode, Object message, Location mComingPickupLocation) {
        Bundle bundle = new Bundle();
        if (message instanceof String) {
            bundle.putString(RESULT_KEY, (String) message);
            if(resultCode==RESULT_FAILURR) {
                bundle.putParcelable(LOCATN_KEY, mLastLocation);
            }
        } else {
            bundle.putParcelableArrayList(RESULT_KEY, (ArrayList<? extends Parcelable>) message);
        }
        bundle.putParcelable("Location", mComingPickupLocation);
        mReceiver.send(resultCode, bundle);
    }
}