package com.hztech.singapore.managers;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.location.places.Places;
import com.google.gson.Gson;
import com.hztech.singapore.R;
import com.hztech.singapore.interfaces.LocationSettingsRequestsInterface;
import com.hztech.singapore.interfaces.OperationListener;
import com.hztech.singapore.utilities.UtiliyClass;
import com.hztech.singapore.views.CustomMapView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by HzTech on 7/25/2017.
 */
public class LocationManager  implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationSettingsRequestsInterface, LocationListener {

    public Context getContext() {
        return mContext;
    }

    Context mContext;
    GoogleApiClient mGoogleApiClient;
    LocationRequest mLocationRequest;
    private CustomMapView mMapView;
    private static LocationManager mManger;
    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 1000;
    private boolean mTripStarted = false;
    private Location mLastLocation;
    private String mCountryLocale,mCityLocale;
    private GeocodingResultReceiver mResultReceiver = GeocodingResultReceiver.getInstance();
    private OperationListener operationListener;
    private OperationListener GeocodeOperationFailureListener;
    private OperationListener countryOperationListener = new OperationListener() {
        @Override
        public void onOperationCompleted(int resultCode, Object mComingValue, Context mContext) {
            if (resultCode == 1) {
                mCountryLocale = ((String) mComingValue).split(",")[0];
                mCityLocale = ((String) mComingValue).split(",")[1].trim();
                String mCountryCode = PreferenceManager.getDefaultSharedPreferences(mContext).getString("COUNTRYCODE", "");
                String mCity = PreferenceManager.getDefaultSharedPreferences(mContext).getString("CITY", "");
                if (!mCountryCode.equalsIgnoreCase(mCountryLocale)) {
                    PreferenceManager.getDefaultSharedPreferences(mContext).edit().putString("COUNTRYCODE", mCountryLocale).apply();
                    Log.d(this.getClass().getSimpleName(), "================= country set " + mCountryLocale);
                }
//                if (!mCity.equalsIgnoreCase(mCityLocale)) {
                    PreferenceManager.getDefaultSharedPreferences(mContext).edit().putString("CITY", mCityLocale).apply();
                    Log.d(this.getClass().getSimpleName(), "================= city set " + mCityLocale);
//                }
                if(operationListener!=null){
                    operationListener.onOperationCompleted(3,getmLastLocation(),null);
                }
            }
        }
    };

    public Location getmLastLocation() {
        return mLastLocation;
    }

    public String getmCountryLocale() {
        mCountryLocale= PreferenceManager.getDefaultSharedPreferences(mContext).getString("COUNTRYCODE", "eg");
        return mCountryLocale;
    }

    public String getmCityLocale() {
        mCityLocale= PreferenceManager.getDefaultSharedPreferences(mContext).getString("CITY", "");
        return mCityLocale;
    }
    public static LocationManager getInstance(Context mComingContex) {

        if (mManger == null) {
            mManger = new LocationManager(mComingContex);
        }
        mManger.mContext = mComingContex;
        return (LocationManager) mManger;
    }

    private LocationManager(Context mComingContext) {
        mContext=mComingContext;
    }

    public LocationManager buildGoogleMapApiClient(OperationListener mComingOperationListener) {              //*************1st called
        if (mGoogleApiClient == null) {
            if (checkPlayServices()) {
                mGoogleApiClient = new GoogleApiClient.Builder(mContext)
                        .addConnectionCallbacks(this)
                        .addOnConnectionFailedListener(this)
                        .addApi(LocationServices.API)
                        .addApi(Places.GEO_DATA_API)
                        .build();
            }
        }

        if (mGoogleApiClient != null) {
            if (!mGoogleApiClient.isConnected()) {
                mGoogleApiClient.connect();
            } else {
                CheckLocationSettingsAPI();
            }
        }

        operationListener = mComingOperationListener;
        return mManger;
    }

    private boolean checkPlayServices() {
        if (UtiliyClass.checkForInternetConnectivity(getContext())) {
            int resultCode = GoogleApiAvailability.getInstance()
                    .isGooglePlayServicesAvailable(mContext);
            if (resultCode != ConnectionResult.SUCCESS) {
                if (GoogleApiAvailability.getInstance().isUserResolvableError(resultCode)) {
                    GoogleApiAvailability.getInstance().getErrorDialog((Activity) mContext, resultCode,
                            PLAY_SERVICES_RESOLUTION_REQUEST).show();
                } else {
                    UtiliyClass.showSnackToast(mContext.getString(R.string.this_device_notsupport),mContext);
                }
                return false;
            }
        } else {
            //Toast.makeText(getContext(), "Make Sure Internet Connection is On", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }


    public void onDestroy() {
        onPause();
        if (mGoogleApiClient != null) {
            mGoogleApiClient.disconnect();
        }
    }

    public void onPause() {
        if (mGoogleApiClient != null) {
            if (mGoogleApiClient.isConnected()) {
                LocationServices.FusedLocationApi.removeLocationUpdates(
                        mGoogleApiClient, this);
            }
        }
    }

    @Override
    public void onConnected(Bundle bundle) {        //***************2nd called
        CheckLocationSettingsAPI();
    }

    private Location requestLastLocationWithAPICheck() {
        Location mLocation = null;
        if (Build.VERSION.SDK_INT >= 23) {
            if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED
                    || ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                mLocation = LocationServices.FusedLocationApi.getLastLocation(
                        mGoogleApiClient);

            }
        } else {
            mLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        }
        return mLocation;
    }

    public LatLngBounds getBounds(double mDistanceInKilos) {
        double latRadian;
        try {
            latRadian   = Math.toRadians(mLastLocation.getLatitude());
        }catch(NullPointerException exception){
            latRadian=0;
        }
        double degLatKm = 110.574235;
        double degLngKm = 110.572833 * Math.cos(latRadian);
        double deltaLat = mDistanceInKilos / degLatKm;
        double deltaLong = mDistanceInKilos / degLngKm;

        double minLat = mLastLocation.getLatitude() - deltaLat;
        double minLong = mLastLocation.getLongitude() - deltaLong;
        double maxLat = mLastLocation.getLatitude() + deltaLat;
        double maxLong = mLastLocation.getLongitude() + deltaLong;

        return new LatLngBounds(new LatLng(minLat, minLong), new LatLng(maxLat, maxLong));
    }

    private void updateLocation() {
        if (mGoogleApiClient == null || !mGoogleApiClient.isConnected()) {
            buildGoogleMapApiClient(operationListener);
        } else {
            startLocationUpdate();
        }
    }

    private void updateLocation(Location mLocation) {          //********* 4th called multiple time, when GoogleApiClient connected, when LocationSettings updated or when Requesting LocationSetting update and when location update change
//        if (mLocation == null) {
//            mLastLocation = requestLastLocationWithAPICheck();
//        } else {
        mLastLocation = mLocation;
//        }
//        if (mLastLocation != null) {
//            startGeocodingService(mLastLocation, "", true);
//        }
        if (mLastLocation != null) {
            mMapView.getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(GoogleMap googleMap) {
                    if (operationListener != null) {
                        operationListener.onOperationCompleted(1, mLastLocation, getContext());
                    } else {
                        googleMap.setMyLocationEnabled(true);
//                        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude()), 17.0f));
                    }
                }
            });
//            startLocationUpdate();
//            if (Geocoder.isPresent()) {
//            startGeocodingService(mLastLocation, "");
//            } else {
//                Toast.makeText(getContext(), "Geocoder is not present", Toast.LENGTH_SHORT).show();
//            }
        } else {
            CheckLocationSettingsAPI();
        }
    }

    private void startLocationUpdate() {
        if (mLocationRequest == null) {
            createLocationRequest();
        }
        if (Build.VERSION.SDK_INT >= 23) {
            if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED
                    || ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
            }
        } else {
            if (mGoogleApiClient.isConnected())
                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        GooglePlayServicesUtil.getErrorDialog(connectionResult.getErrorCode(), (Activity) getContext()
                , PLAY_SERVICES_RESOLUTION_REQUEST).show();
    }

    private void CheckLocationSettingsAPI() {           //**********3rd called when settings Not available

        createLocationRequest();
        LocationSettingsRequest.Builder mBuilder = new LocationSettingsRequest.Builder().addLocationRequest(mLocationRequest);
        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, mBuilder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
//                final LocationSettingsStates mstatus = result.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        // All location settings are satisfied. The client can initialize location
                        // requests here.
//                        Toast.makeText(getContext(), "acquiring Location ... ", Toast.LENGTH_SHORT).show();
//                        LocationAvailability mLocationAvailableResult = LocationServices.FusedLocationApi.getLocationAvailability(mGoogleApiClient);
//                        if (mLocationAvailableResult.isLocationAvailable()) {
                        if (Build.VERSION.SDK_INT >= 23) {
                            if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED
                                    || ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                                mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                                        mGoogleApiClient);
                            } else {
                                mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                                        mGoogleApiClient);
                            }
                        }
//                        if (mLastLocation != null) {
//                            updateLocation();
//                        } else {
                        updateLocation();
//                        }
//                        } else {
//                            Toast.makeText(getContext(), "Location Not available unable gps ", Toast.LENGTH_SHORT).show();
//                        }
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied. But could be fixed by showing the user
                        // a dialog.
//                        Toast.makeText(getContext(), "Location Settings Not completed ", Toast.LENGTH_SHORT).show();
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(
                                    (Activity) getContext(),
                                    5000);
                        } catch (IntentSender.SendIntentException e) {
                            // Ignore the error.
                            Log.e(this.getClass().getSimpleName(), "exception resolution required");
//                            Toast.makeText(getContext(), "exceptionin RESOLUTION_REQUIRED ", Toast.LENGTH_SHORT).show();
//                            ((HomeActivity)getContext()).showSnackToast();
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have no way to fix the
                        // settings so we won't show the dialog.
//                        Toast.makeText(getContext(), "GPS not enabled", Toast.LENGTH_SHORT).show();
                        UtiliyClass.showSnackToast(mContext.getString(R.string.gps_not_enabled),mContext);
//                        startLocationUpdate();
                        break;
                }
            }
        });

    }

    private void createLocationRequest() {       //********** Called When No LocationRequests
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(3000);
        mLocationRequest.setFastestInterval(3000);
//        mLocationRequest.setSmallestDisplacement(50.0f);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);

    }

    public LocationManager setMapView(MapView mComingMapView) {
        mMapView = (CustomMapView) mComingMapView;
        if (mMapView != null) {

        }
        return (LocationManager) mManger;
    }

    @Override
    public void onRequestResult(int requestCode, int resultCode, Intent Date) {      //Called When LocationSettings Update
        if (requestCode == 5000) {
            switch (resultCode) {
                case Activity.RESULT_OK:
//                    updateLocation(null);
                    mMapView.getMapAsync(new OnMapReadyCallback() {
                        @Override
                        public void onMapReady(GoogleMap googleMap) {
                            googleMap.setMyLocationEnabled(true);
                            if (getmLastLocation() != null)
                                googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(getmLastLocation().getLatitude(), getmLastLocation().getLongitude()), 17.0f));

                        }
                    });
                    break;
                case Activity.RESULT_CANCELED:
//                CheckLocationSettingsAPI();
                    //Toast.makeText(getContext(), "GPS not enabled", Toast.LENGTH_SHORT).show();
                    UtiliyClass.showSnackToast(getContext().getString(R.string.gps_not_enabled),getContext());
//                    startLocationUpdate();
                    break;
            }
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        updateLocation(location);
    }

    public void setOperationListener(OperationListener operationListener) {
        this.operationListener = operationListener;
    }

    public void setGeocodeOperationFailureListener(OperationListener operationListener) {
        this.GeocodeOperationFailureListener = operationListener;
    }

    boolean movestarted = false;

    public void startLocationGeocodingService(MapView mMapView) {
        mMapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(final GoogleMap googleMap) {
//                googleMap.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
//                    @Override
//                    public void onCameraChange(CameraPosition cameraPosition) {
//
//                        Location mLocationToGeo = new Location("");
//                        mLocationToGeo.setLatitude(cameraPosition.target.latitude);
//                        mLocationToGeo.setLongitude(cameraPosition.target.longitude);
//                        startGeocodingService(mLocationToGeo, "");
//
//                    }
//                });
                googleMap.setOnCameraMoveStartedListener(new GoogleMap.OnCameraMoveStartedListener() {
                    @Override
                    public void onCameraMoveStarted(int i) {
//                        if(i== GoogleMap.OnCameraMoveStartedListener.REASON_GESTURE||i==GoogleMap.OnCameraMoveStartedListener.REASON_DEVELOPER_ANIMATION){
                        if (!movestarted)
                            movestarted = true;
//                        }
                    }
                });

//                googleMap.setOnCameraMoveListener(new GoogleMap.OnCameraMoveListener() {
//                    @Override
//                    public void onCameraMove() {
//                        if(!movestarted)
//                            movestarted = true;
//                    }
//                });

                googleMap.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
                    @Override
                    public void onCameraIdle() {
//                        if (movestarted) {
                        LatLng mCurrentLocatio = googleMap.getCameraPosition().target;
                        Location mLocation = new Location("");
                        mLocation.setLatitude(mCurrentLocatio.latitude);
                        mLocation.setLongitude(mCurrentLocatio.longitude);
                        startGeocodingService(mLocation, "", false);
                        movestarted = false;
//                        }
                    }
                });
            }
        });

    }

    public void setCountryLocale(String countryLocale) {
        this.mCountryLocale = countryLocale;
    }

    public interface UpdateJurneyListener {
        public void updateJurney(int mComingArrayName);
    }

    public void startGeocodingService(Location mComingLocation, String mComingText, boolean countrylocaleRequest) {
        mResultReceiver.setOperationListener(operationListener);

        Intent mIntent = new Intent(getContext(), GeocodingService.class);
        mIntent.putExtra(GeocodingService.RECIVR_KEY, mResultReceiver);
        mIntent.putExtra(GeocodingService.LOCATN_KEY, mComingLocation);
        mIntent.putExtra(GeocodingService.TEXT_KEY, mComingText);
        if (mComingLocation == null && mComingText != null) {
            mIntent.setAction(GeocodingService.TEXT_KEY);
        } else {
            mIntent.setAction(GeocodingService.LOCATN_KEY);
            if (countrylocaleRequest) {
                mIntent.setAction("CountryGeocode");
            }
        }
        getContext().startService(mIntent);
    }

    public OperationListener getCountryOperationListener() {
        return this.countryOperationListener;
    }

    public GoogleApiClient getGoogleApiClient() {
        return mGoogleApiClient;
    }

    public void verifyCity(Location comingLocation, boolean callforGeocode, final OperationListener comingListener) {
        String mURL = "http://maps.googleapis.com/maps/api/geocode/";

        Retrofit mCity = new Retrofit.Builder().baseUrl(mURL).client(new OkHttpClient.Builder().addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY)).build())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
String language="en";
        GetCity mCityInterface = mCity.create(GetCity.class);
        Call<Object> mCityGetter = mCityInterface.getCity(comingLocation.getLatitude() + "," + comingLocation.getLongitude(), true + "",language);
        mCityGetter.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                ////                Log.v(this.getClass().getSimpleName(), "LocationMessaage" + response.body());
//                ParserTask mTask = new ParserTask(ContactPassengerFragment.this);
//                mTask.execute(response.body());
                JSONObject jsonObj = null;
                try {
                    jsonObj = new JSONObject(new Gson().toJson(response.body()));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if(jsonObj!=null)
                Log.i("JSON string =>", jsonObj.toString());

                String Address1 = "";
                String Address2 = "";
                String City = "";
                String State = "";
                String Country = "";
                String County = "";
                String PIN = "";

                String currentLocation = "";

                try {
                    String status = jsonObj.getString("status").toString();
                    Log.i("status", status);

                    if (status.equalsIgnoreCase("OK")) {
                        JSONArray Results = jsonObj.getJSONArray("results");
                        JSONObject zero = Results.getJSONObject(0);
                        JSONArray address_components = zero
                                .getJSONArray("address_components");

                        for (int i = 0; i < address_components.length(); i++) {
                            JSONObject zero2 = address_components.getJSONObject(i);
                            String long_name = zero2.getString("long_name");
                            JSONArray mtypes = zero2.getJSONArray("types");
                            String Type = mtypes.getString(0);

                            if (Type.equalsIgnoreCase("street_number")) {
                                Address1 = long_name + " ";
                            } else if (Type.equalsIgnoreCase("route")) {
                                Address1 = Address1 + long_name;
                            } else if (Type.equalsIgnoreCase("sublocality")) {
                                Address2 = long_name;
                            } else if (Type.equalsIgnoreCase("locality")||Type
                                    .equalsIgnoreCase("administrative_area_level_1")) {
                                // Address2 = Address2 + long_name + ", ";
                                City = long_name;
                            } else if (Type
                                    .equalsIgnoreCase("administrative_area_level_2")) {
                                County = long_name;
                            } else if (Type
                                    .equalsIgnoreCase("administrative_area_level_1")) {
                                State = long_name;
                            } else if (Type.equalsIgnoreCase("country")) {
                                Country = long_name;
                            } else if (Type.equalsIgnoreCase("postal_code")) {
                                PIN = long_name;
                            }

                        }

                        currentLocation = Address1 + "," + Address2 + "," + City + ","
                                + State + "," + Country + "," + PIN;
                        if(City!=null&&!City.isEmpty()) {
                            Log.d(this.getClass().getSimpleName(),"======================= CITY" +City);
                            PreferenceManager.getDefaultSharedPreferences(mContext).edit().putString("CITY", City).apply();
                            comingListener.onOperationCompleted(1,currentLocation,null);
                        }else if(State!=null&&!State.isEmpty()){
                            Log.d(this.getClass().getSimpleName(),"======================= CITY" +State);
                            PreferenceManager.getDefaultSharedPreferences(mContext).edit().putString("CITY", State).apply();
                            comingListener.onOperationCompleted(1,currentLocation,null);
                        }

                    }
                } catch (Exception e) {

                }
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                Log.v(this.getClass().getSimpleName(), "LocationMessaage" + t.getMessage());
                comingListener.onOperationCompleted(0,getContext().getString(R.string.service_not_available),null);
            }
        });

    }

    public interface GetCity {
        @GET("json")
        retrofit2.Call<Object> getCity(@Query("latlng") String mComingOrigin, @Query("sensor") String mComingDestination, @Query("language") String cominglanguage);
    }
}