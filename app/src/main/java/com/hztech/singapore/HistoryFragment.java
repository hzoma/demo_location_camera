package com.hztech.singapore;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.hztech.singapore.DAL.Attendence;
import com.hztech.singapore.DAL.RealmUtil;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link HistoryFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class HistoryFragment extends Fragment {

    View mView;
    ListView mListView;

    public HistoryFragment() {
        // Required empty public constructor
    }


    public static HistoryFragment newInstance() {
        HistoryFragment fragment = new HistoryFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_history, container, false);
        mListView = (ListView) mView.findViewById(R.id.ls_listView);

        ArrayList<Attendence> mAttendenceList = RealmUtil.getAttendencess();
        AttendenceAdapter mAdapter = new AttendenceAdapter(getActivity(), mAttendenceList);
        mListView.setAdapter(mAdapter);
        return mView;
    }

    class AttendenceAdapter extends BaseAdapter {
        Context mContext;
        ArrayList<Attendence> mAttendence;

        public AttendenceAdapter(Context comingContext, ArrayList comingList) {
            mContext=comingContext;
            mAttendence=comingList;
        }

        @Override
        public int getCount() {
            return mAttendence.size();
        }

        @Override
        public Object getItem(int i) {
            return mAttendence.get(i).getmTime();
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            if(view==null){
                view= LayoutInflater.from(mContext).inflate(R.layout.list_item,null);
                view.setTag(view);
            }else{
                view=(View)view.getTag();
            }

            SimpleDateFormat mFormatter=new SimpleDateFormat("dd/MM/yyyy");
            String mDate=mAttendence.get(i).getmTime();
            Calendar mCalender=Calendar.getInstance();
            mCalender.setTimeInMillis(Long.parseLong(mDate));
            ((TextView)view.findViewById(R.id.time)).setText(mFormatter.format(mCalender.getTime()));
            ((TextView)view.findViewById(R.id.location)).setText(mAttendence.get(i).getLocation());
            ((TextView)view.findViewById(R.id.camera)).setText(mAttendence.get(i).getmCamera());
            return view;
        }
    }
}
