package com.hztech.singapore.interfaces;

import android.content.Intent;

/**
 * Created by Hzkaram on 7/25/2017.
 */
public interface LocationSettingsRequestsInterface {
    void onRequestResult(int requestCode, int resultCode, Intent Date);
}
