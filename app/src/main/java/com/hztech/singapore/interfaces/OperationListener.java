package com.hztech.singapore.interfaces;

import android.content.Context;

/**
 * Created by HzKaram on 7/25/2017.
 */
public interface OperationListener{
    public void onOperationCompleted(int resultCode, Object mComingValue, Context mContext);
}